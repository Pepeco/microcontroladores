#include <SFR51.inc>
cseg at 0

location EQU 2000h ; ponto de montagem

ljmp  startup

;-------------------------------------------------
ORG location; endereco inicial de montagem
;-------------------------------------------------

;
;Cabeçalho do Paulmon2 ---------------------------
;
DB  A5h,E5h,e0h,A5h    ;signiture bytes
DB  35,255,0,0             ;id (35=prog, 253=startup, 254=command)
DB  0,0,0,0                ;prompt code vector
DB  0,0,0,0                ;reservado
DB  0,0,0,0                ;reservado
DB  0,0,0,0                ;reservado
DB  0,0,0,0                ;definido pelo usuário
DB  255,255,255,255        ;tamanho e checksum (255=não usado)
DB  "leds4",0              ;maximo 31 caracteres  mais o zero

ORG location+40            ;endereço de montagem do código executável
                           ; (64)[b10] == (40)(b16) 
;
;rotinas do paulmon2 -----------------------------
;
cout         EQU 0030h ;imprime o acumulador na serial
cin          EQU 0032h ;captura para o acumulador o que vem da serial
esc          EQU 003Eh ;Checagem da tecla ESC do paulmon2

port_1       EQU 90h
;
;Subrotinas locais -------------------------------
;

;delay for a number of ms (specified by acc)
delay:
        mov r0,#10
dly2:   mov r1,#250
dly3:   mov r2,#250

dly4:
        nop
        nop
        nop
        nop
        nop
        nop
        djnz    r2,dly4       ;repete r1 vezes
        djnz    r1,dly3       ;repete r0 vezes
        djnz    r0,dly2

        ret

update:                 ;Atualiza a configurao dos LEDs
    ;push  dph          ;guarda o dptr
    ;push  dpl          
    
    ;mov dptr,#port_1   ;dptr -> a constante port_1(90h) 
    ;movx  @dptr,a
    mov  P1, a

    mov a, #FFh
    acall delay       ;gasta tempo
    acall delay       ;gasta tempo
    acall delay       ;gasta tempo
    
    ;pop   dpl          ;recupera o dptr
    ;pop   dph  
    
    ret

;
;Programa principal-------------------------------
;
startup:
  
begin:  
    setb  TI;<<<<<<<<<<<<<<<<<<<< simulacao LIGADA
    mov   dptr,#tabela  ; passa o conte�do da tabela para dptr

  loop:
    clr   a           ;a <- 0

    movc  a,@a+dptr   ;a aponta para dptr
    jz begin          ;se for 0, volta para begin
    acall update      ;chama update
    inc dptr          ;incrementae em um o conte�do de dptr
    sjmp  loop        ;comeca de novo

  exit:
    ret              ;retorna ao PAULMON2

tabela:; gera agrupamentos de 8 bits

  DB 11110111b
  DB 11111011b
  DB 11111101b
  DB 11111110b
  DB 255,0
  
END
